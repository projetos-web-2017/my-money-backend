const restful = require('node-restful');
const mongoose = restful.mongoose;

const pessoaSchema = new mongoose.Schema({
    cpf: { type: String, required: true },          // ok
    foto: { type: String, },                        // ok
    nome: { type: String, required: true },         // ok
    cep: { type: String, },                         // ok
    codMunicipio: { type: String, },                // ok
    codLogradouro: { type: String, },               // ok 
    logradouro: { type: String, },                  // ok
    numero: { type: String, },                      // ok
    complemento: { type: String, },                 // ok
    bairro: { type: String, },                      // ok
    telefoneCelular: { type: String, },             // ok
    telefoneFixo: { type: String, },                // ok
    //-------------------------------------
    empresa: { type: String, required: true },      // ok
    departamento: { type: String, required: true }, // ok
    //--------------------------------------
    email: { type: String, required: true },        // ok
    senha: { type: String, required: true }         // ok
});

module.exports = restful.model('PessoaSchema', pessoaSchema);
