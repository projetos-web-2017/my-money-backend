const express = require('express');
const auth = require('./auth')

module.exports = server => {
    // //URL base para todas as rodas
    // const router = express.Router();
    // server.use('/api', router);


    /**
     * Rotas protegidas
     */
    const protectedApi = express.Router();
    server.use('/api', protectedApi)

    protectedApi.use(auth);

    // Rotas do ciclo de pagamento
    const BillingCycle = require('../api/billingCycle/billingCycleService');
    const Pessoa = require('../api/pessoa/pessoaService');
    
    BillingCycle.register(protectedApi, '/billingCycles');
    Pessoa.register(protectedApi, '/pessoas');   
    

    /**
     * Rotas abertas
     */

    const openApi = express.Router();
    server.use('/oapi', openApi);

    const AuthService = require('../api/user/AuthService')
    openApi.post('/login', AuthService.login)
    openApi.post('/signup', AuthService.signup)
    openApi.post('/validateToken', AuthService.validateToken)


};
